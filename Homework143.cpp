#include <iostream>
#include <ctime>

int main()
{ 
	const int N = 6;
	
	int array[N][N];

	// Part 1 and 2
	std::cout << "Array:\n";
	for (int i = 0; i < N; ++i) {

		for (int j = 0; j < N; ++j) {
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << '\n';
	}

	// Part 3 
	int day{ 26 };
	int index = day % N;
	int sum { 0 };
	for (int j = 0; j < N; ++j) {
		sum += array[index][j];
	}
	std::cout << "Sum of elements in row " << index << " - " << sum << '\n';

}
